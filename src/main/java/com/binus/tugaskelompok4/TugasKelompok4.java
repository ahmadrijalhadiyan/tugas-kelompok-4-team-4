/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.binus.tugaskelompok4;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class TugasKelompok4 {
    private static char opsi;
    private static Scanner input = new Scanner(System.in);
    private static int [] randomdata = new int[5];

    public static void main(String[] args) {
         while (true){
            Menu();
        }
    }
    
    private static void Menu(){
    	System.out.println("\n\n\t     Selamat Datang di Program Simulasi");
        System.out.println("\nMenu : "
                + "\n 1. Random Data"
                + "\n 2. Simulasi Bubble Sort - Ascending"
                + "\n 3. Simulasi Selection Sort - Ascending"
                + "\n 4. Simulasi Bubble Sort - Descending"
                + "\n 5. Simulasi Selection Sort - Descending"
                + "\n 6. Keluar");
        System.out.print("\nMasukkan Pilihan Anda :  ");
        opsi = input.next().charAt(0);

        switch (opsi){
            case '1':
                randomData();
                System.out.println("\n");
                System.out.println("\nMohon Tekan Enter untuk Mengulangi Kembali... ");
                breakConsole();
                break;
            case '2':
                bubbleAscending(randomdata, 5);
                System.out.println("\n");
                System.out.println("\nMohon Tekan Enter untuk Mengulangi Kembali... ");
                breakConsole();
                break;
            case '3':
                selectAscending(randomdata, 5);
                System.out.println("\n");
                System.out.println("\nMohon Tekan Enter untuk Mengulangi Kembali... ");
                breakConsole();
                break;
            case '4':
                bubbleDescending(randomdata, 5);
                System.out.println("\n");
                System.out.println("\nMohon Tekan Enter untuk Mengulangi Kembali... ");
                breakConsole();
                break;
            case '5':
                selectDescending(randomdata, 5);
                System.out.println("\n");
                System.out.println("\nMohon Tekan Enter untuk Mengulangi Kembali... ");
                breakConsole();
                break;
            case '6':
                System.exit(0);
                break;
            default:
                System.out.print("Masukkan pilihan antara 1 - 6");
                break;
        }

    }
    
     private static void randomData(){
    	
    	 System.out.println("\n=================");
         System.out.println("Random Data");
         System.out.println("===================");
         System.out.print("Masukkan Batas Bawah : ");
         int bts_bawah=input.nextInt();
         input.nextLine();
         System.out.print("Masukkan Batas Atas : ");
         int bts_atas=input.nextInt();
         
        if(bts_bawah >= bts_atas)
            System.out.print("Batas Atas harus lebih besar dari batas bawah");
        else {
            int range = bts_atas - bts_bawah;
            for(int i = 0; i < 5; i++){
                randomdata[i] = (int)Math.floor(Math.random()*(range+1)+bts_bawah);
                System.out.print(randomdata[i] + " " );
            }
        }
        
    }

    private static void bubbleAscending(int [] data, int totalData){
        System.out.println("\n==================================");
        System.out.println("Simulasi Bubble Sort - Ascending");
        System.out.println("====================================");
        // data sebelum sorting
        System.out.print("\nData sebelum di sorting: ");
        for(int i = 0 ; i < totalData; i++){
            System.out.print(data[i] + " ");
        }

        // proses Bubble sort Ascending
        System.out.println("\n\nProses Bubble Sort Ascending");
        for(int i = 0; i < totalData; i++){
            System.out.println("\n\nPass " + (i + 1) + " :");
            for(int j = 0; j < totalData; j++)
                System.out.print(data[j] + " ");
            	System.out.println();
            	
            for(int j = 0; j < totalData - 1; j++){
                if(data[j] > data[j + 1]){
                    int temp = data[j];
                    data[j] = data[j + 1];
                    data[j + 1] = temp;
                }

                if(j < totalData - (i + 1)){
                    for(int k = 0; k < totalData; k++)
                        System.out.print(data[k] + " ");
                    	System.out.println();
                }
                
            }
            System.out.println("Result of Pass " + (i + 1) + " :");
            for(int j = 0; j < totalData; j++)
                System.out.print(data[j] + " ");
        }
    }

    private static void selectAscending(int [] data, int totalData){
        System.out.println("\n==================================");
        System.out.println("Simulasi Selection Sort - Ascending");
        System.out.println("====================================");
        // data sebelum di sorting
        System.out.print("\nData sebelum disorting: ");
        for(int i = 0; i < totalData; i++)
            System.out.print(data[i] + " ");

        // proses selection sort Ascending
        System.out.println("\nProses Selection Sort Ascending");
        for(int i = 0; i < totalData - 1; i++){
            System.out.println("\n\nPass " + (i + 1) + " : ");
            for(int j = 0; j < totalData; j++)
                System.out.print(data[j] + " ");

            boolean shift = false;
            int index = 0;
            int min = data[i];
            for(int j = i + 1; j < totalData; j++){
                if(min > data[j]){
                	shift = true;
                    index = j;
                    min = data[j];
                }
            }

            if(shift == true){
                int temp = data[i];
                data[i] = data[index];
                data[index] = temp;
            }
            System.out.println("\nResult of Pass " + (i + 1) + " :");
            for(int j = 0; j < totalData; j++)
                System.out.print(data[j] + " ");
        }
    }

    private static void bubbleDescending(int [] data, int totalData){
        // data sebelum sorting
        System.out.print("\nData sebelum di sorting: ");
        for(int i = 0 ; i < totalData; i++){
            System.out.print(data[i] + " ");
        }

        // proses Bubble sort Descending
        System.out.println("\n\nProses Bubble Sort Descending");
        for(int i = 0; i < totalData; i++){
            System.out.println("\n\nPass " + (i + 1) + " :");
            for(int j = 0; j < totalData; j++)
                System.out.print(data[j] + " ");
            	System.out.println();
            for(int j = 0; j < totalData - 1; j++){
                if(data[j] < data[j + 1]){
                    int temp = data[j];
                    data[j] = data[j + 1];
                    data[j + 1] = temp;
                }

                if(j < totalData - (i + 1)){
                    for(int k = 0; k < totalData; k++)
                        System.out.print(data[k] + " ");
                    	System.out.println();
                }
                
            }
            System.out.println("Result of Pass " + (i + 1) + " :");
            for(int j = 0; j < totalData; j++)
                System.out.print(data[j] + " ");
        }
    }

    private static void selectDescending(int [] data, int totalData){
        // data sebelum di sorting
        System.out.print("\nData sebelum disorting: ");
        for(int i = 0; i < totalData; i++)
            System.out.print(data[i] + " ");

        // proses selection sort Descending
        System.out.print("\nProses Selection Sort Descending");
        for(int i = 0; i < totalData - 1; i++){
            System.out.println("\n\nPass " + (i + 1) + " : ");
            for(int j = 0; j < totalData; j++)
                System.out.print(data[j] + " ");

            boolean shift = false;
            int index = 0;
            int min = data[i];
            for(int j = i + 1; j < totalData; j++){
                if(min < data[j]){
                	shift = true;
                    index = j;
                    min = data[j];
                }
            }

            if(shift == true){
                int temp = data[i];
                data[i] = data[index];
                data[index] = temp;
            }
            System.out.println("\nResult of Pass " + (i + 1) + " :");
            for(int j = 0; j < totalData; j++)
                System.out.print(data[j] + " ");
        }
    }
    private static void breakConsole(){
        try{
            System.in.read();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
